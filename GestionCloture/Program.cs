﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.Sql;
using System.Timers;

namespace GestionCloture
{
    /// <summary>
    /// Classe Principale du programme
    /// Permet de modifier l'état des fiches visiteurs dans la base de données à un intervale régulier
    /// </summary>
    class Program
    {
        //instancie un Timer
        private static System.Timers.Timer aTimer;

        //initialisation des identifiants pour la connexion à la base de données (ici en ligne)
        private static string serveur = "freedb.tech";
        private static string port = "3306";
        private static string basedonnees = "freedbtech_gsbfrais";
        private static string utilisateur = "freedbtech_userGsb";
        private static string mdp = "secret";
        /*
        // identifiants pour utilisation en local:
        private static string serveur = "localhost";
        private static string port = "3306";
        private static string basedonnees = "gsb_frais";
        private static string utilisateur = "userGsb";
        private static string mdp = "secret";
        */
        

        //Bloc éxécuté, lance le timer
        /// <summary>
        /// Bloc principal, éxécuté
        /// Lance le timer
        /// </summary>
        static void Main(string[] args)
        {
            SetTimer();
            Console.ReadLine();
            aTimer.Stop();
            aTimer.Dispose();
        }

        //Créer un timer qui appelle un évenement avec un intervale de 2 secondes
        /// <summary>
        /// Créer un timer qui appelle un évenement avec un intervale de 2 secondes
        /// </summary>
        private static void SetTimer()
        {
            // Création d'un timer avec un intervale de 10 secondes
            aTimer = new System.Timers.Timer(10000);
            //Ajout d'un évènement à éxecuter à chaque intervale 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        // S'éxécute à chaque intervale du timer. Evenement qui appelle les methodes de connexion à la bdd, et qui modifie l'état des fiches en fonction du jour du mois
        /// <summary>
        /// Evenement qui appelle les methodes de connection à la bdd <see cref="AccesBdd.AccesBdd(string)"/>, et vérifie qu'elle s'est bien effectuée <see cref="AccesBdd.GetSuccesConnexion()"/>
        /// modifie l'état des fiches avec une requete update <see cref="AccesBdd.Requete(string)"/> en fonction du jour <see cref="GestionDates.Entre(int, int)"/> 
        /// et ferme la connextion pour ne pas surcharger <see cref="AccesBdd.FermerConnexion()"/>
        /// </summary>
        /// <remarks> S'éxécute à chaque intervale du timer.</remarks>
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //Connexion à la bdd de gsb
            AccesBdd connexionGsb = new AccesBdd("SERVER="+serveur+"; port="+port+"; DATABASE="+basedonnees+"; UID="+utilisateur+"; PASSWORD=" + mdp);            

            //Console.WriteLine("ONTIMEDEVENT REUSSI ");
            if (connexionGsb.GetSuccesConnexion())
            {
                //Test Requete Select:
                //Console.WriteLine(connexionGsb.RequeteSelect("SELECT idvisiteur, mois FROM fichefrais WHERE idetat = 'RB' AND mois = '202103'")[0][0]);
                // Test recupération d'un champ:
                //Console.WriteLine(connexionGsb.GetChamp("idvisiteur", "fichefrais", false)[0]);

                //Entre 1er et 10 du mois lors de la campagne de validation, on dois cloturer (CL) toutes les fiches crées (CR) le mois précédent
                if (GestionDates.Entre(1, 10))
                {
                    //On récupère le mois précédent et son année
                    string mois = GestionDates.GetMoisPrecedent();
                    string dateModif = GestionDates.GetDateModif();
                    Console.WriteLine("Cloture des fiches mois: "+mois);
                    //Mise a jour des fiches crées (CR) vers l'état cloturé (CL)
                    connexionGsb.Requete("update fichefrais set idetat='CL', datemodif='" + dateModif + "' where mois =" + mois + " and idetat='CR'");
                }
                //A partir du 20 du mois on souhaite mettre en remboursement (RB) toutes les fiches validées (VA) du mois precedent
                else if (GestionDates.Entre(20, 31))
                {
                    //Récupération du mois précédent l'actuel (au format yyyymm)
                    string mois = GestionDates.GetMoisPrecedent();
                    string dateModif = GestionDates.GetDateModif();
                    Console.WriteLine("Mise en remboursement des fiches: "+mois);
                    //Mise a jour des fiches validées (VA) vers l'état remboursé (RB)
                    connexionGsb.Requete("update fichefrais set idetat='RB', datemodif='" + dateModif + "' where mois = " + mois + " and idetat='VA'");
                }
                //ferme la connexion à la base de données entre chaque intervale du timer
                connexionGsb.FermerConnexion();
            }
            else
            {
                Console.WriteLine("\nLa maj des fiches n'a pas pu être opérée. Assurez vous d'être bien connecté.e au réseau et que la bdd exite");
            }
             
        }

    }
}
