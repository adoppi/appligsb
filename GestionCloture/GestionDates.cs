﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.Sql;

namespace GestionCloture

{   /// <summary>
    /// Classe de gestion des dates.
    /// Permet de trouver les mois précédent et suivant le mois actuel, et de chercher si on est bien entre deux dates données
    /// </summary>
    public abstract class GestionDates
    {
        // Methode qui donne le mois précédent le mois actuel, appelle une surcharge de cette methode en donnant comme paramètre la date d'aujourd'hui
        /// <summary>
        /// Appelle une surcharge de cette methode, en donnant comme paramètre la date d'aujourd'hui
        /// </summary>
        /// <remarks>
        /// Ne prend aucun paramètre
        /// </remarks>
        /// <returns>
        /// Le mois précédent le mois actuel (string au format yyyymm)
        /// </returns>
        static public string GetMoisPrecedent()
        {
            //appel de la surcharge qui prend comme paramètre la date d'aujourd'hui
            return GetMoisPrecedent(DateTime.Today); 
        }

        // Surcharge de la méthode GetMoisPrecedent(), qui recoit un objet DateTime
        /// <summary>
        /// Surcharge de la méthode GetMoisPrecedent(), qui recoit un paramètre DateTime
        /// </summary>
        /// <returns>
        /// Une chaine contenant le mois précédent de la date entrée au format yyyymm
        /// </returns>
        static public string GetMoisPrecedent(DateTime date)
        {            
            date = date.AddMonths(-1);           
            string mois = date.ToString("yyyy") + date.ToString("MM");
            return mois;
        }

        // Methode qui donne le mois suivant le mois actuel, appelle une surcharge de cette methode en donnant comme paramètre la date d'aujourd'hui
        /// <summary>
        /// Donne le mois suivant le mois actuel en appellant une surcharge de cette methode, avec comme paramètre la date d'aujourd'hui
        /// </summary>
        /// <remarks>
        /// Ne prend aucun paramètre
        /// </remarks>
        /// <returns>
        /// Le mois suivant le mois actuel (string au format yyyymm)
        /// </returns>
        static public string GetMoisSuivant()
        {
            //appel de la surcharge qui prend comme paramètre la date d'aujourd'hui
            return GetMoisSuivant(DateTime.Today); 
        }

        // Surcharge de la méthode GetMoisSuivant(), sui recoit un paramètre DateTime
        /// <summary>
        /// Surcharge de la méthode GetMoisSuivant(), qui recoit un paramètre DateTime
        /// </summary>
        /// <returns>
        /// Une chaine contenant le mois suivant de la date entrée au format yyyymm
        /// </returns>
        static public string GetMoisSuivant(DateTime date)
        {
            date = date.AddMonths(+1);
            string mois = date.ToString("yyyy") + date.ToString("MM");
            return mois;
        }

        // Verifie que le jour d'aujourd'hui est compris entre deux autres jours définis
        /// <summary>
        /// Verifie que le jour d'aujourd'hui est compris entre deux autres jours définis
        /// </summary>
        /// <param name="jour1"> Integrer number, borne inférieure de l'intervale défini </param>
        /// <param name="jour2"> Integrer number, borne supérieure de l'intervale défini </param>
        /// <returns>
        /// Le mois suivant le mois actuel (string au format yyyymm)
        /// </returns>
        static public Boolean Entre(int jour1, int jour2)
        {
            return Entre(jour1, jour2, DateTime.Today);
        }

        // Surcharge de la méthode 'Entre', avec un paramètre DateTime en plus 
        /// <summary>
        /// Surcharge de la méthode 'Entre', avec un paramètre DateTime en plus 
        /// </summary>
        /// <param name="jour1"> Integrer number, jour1 entré dans 'Entre()' </param>
        /// <param name="jour2"> Integrer number, jour2 entré dans 'Entre()' </param>
        /// <param name="date"> Date </param>
        /// <returns>
        /// Le résultat d'un test déterminant si la date est comprise entre les deux jours entrés.
        /// </returns>
        static public Boolean Entre(int jour1, int jour2, DateTime date)
        {
            //Sort le jour de la date entrée en paramètre
            int jour = date.Day;
            //Retourne le resultat du test 
            return (jour1 <= jour & jour <= jour2);
        }

        /// <summary>
        /// Pour mettre a jour la date de modifaication dans la bdd, on a besoin de la date d'aujourd'hui au format 'yyyy-mm-dd' 
        /// </summary>
        /// <returns>la date d'aujourd'hui au format 'yyyy-mm-dd'</returns>
        static public string GetDateModif()
        {
            DateTime today = DateTime.Today;
            string dateModif = today.ToString("yyyy") + "-" + today.ToString("MM") + "-" + today.ToString("dd");
            //Console.WriteLine(dateModif);
            return dateModif;
        }
    }
}
