﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.Sql;

namespace GestionCloture
{
    /// <summary>
    /// Classe d'accès aux données
    /// Contient les méthodes nécéssaires à la connection à la base, à l'éxécution d'une requête, et gestion d'un curseur
    /// </summary>
    public class AccesBdd
    {
        // initialisation
        private bool SuccesConnexion; //indique l'état de la connexion (true si connecté, false si la connexion à echoué)
        private MySqlConnection connexion; //gestion d'une connexion à une base de données
        private MySqlCommand commande; //gestion d'une requête à envoyer à la base de données
        private MySqlDataReader curseur; //gestion d'un curseur


        // Constructeur de la classe, créer une connexion à une base de données
        /// <summary>
        /// Constructeur de la classe, créer une connexion à une base de données
        /// Si elle ne se fait pas on intercepte l'erreur et on indique l'etat de la connexion dans SuccesConnexion"
        /// </summary>
        /// <param name="chaineConnexion">Chaine indiquant la localistaion de la base de donnée et les identifiants pour l'y connecter</param>
        public AccesBdd(string chaineConnexion)
        {
            this.connexion = new MySqlConnection(chaineConnexion);
            try
            {
                this.connexion.Open();
                //Console.WriteLine("Connexion reussie");
                SuccesConnexion = true; 
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                SuccesConnexion = false;
            }
        }


        //Methode Get du succès de la connexion
        /// <summary>
        /// Methode Get du succès de la connexion
        /// </summary>
        /// <returns>la variable booléenne privée qui indique si la connexion à réussi à se faire</returns>
        public bool GetSuccesConnexion()
        {
            return SuccesConnexion;
        }


        // Execution d'une requete (insert, update, delete) à définir en paramètre
        /// <summary>
        /// Execution d'une requete (insert, update, delete) à définir en paramètre
        /// </summary>
        /// <param name="chaineRequete">chaine contenant la requete en langage SQL à exécuter</param>
        public void Requete(string chaineRequete)
        {
            this.commande = new MySqlCommand(chaineRequete, this.connexion);
            this.commande.ExecuteNonQuery();
        }


        // Execution d'une requête SELECT et imprime le résultat dans une console
        /// <summary>
        /// Execution d'une requête SELECT et imprime le résultat dans une console
        /// </summary>
        /// <param name="chaineRequete">chaine contenant la requete SELECT en langage SQL à exécuter</param>
        public List<List<string>> RequeteSelect(string chaineRequete)
        {
            this.commande = new MySqlCommand(chaineRequete, this.connexion);
            this.curseur = this.commande.ExecuteReader();
            //crée une liste de liste de chaine qui contiendront les éléments recus de la requ
            List<List<string>> resultat = new List<List<string>>();
            //tant que le curseur n'est pas à la fin on récupère le contenu des champs
            while (curseur.Read())
            {
                int i=0;
                List<string> ligne = new List<string>();
                //On lis le contenu de chaque champ récupéré à chaque étape du curseur
                while (i < curseur.FieldCount)
                {
                    string contenu = curseur.GetString(i);
                    ligne.Add(contenu);
                    Console.WriteLine(curseur.GetName(i)+ " : " + contenu);
                    i++;
                }
                resultat.Add(ligne);
            }
            curseur.Close();
            return resultat;
        }


        // Exécution d'une requete parmettant la récupération d'un champ d'une table avec ou sans doublons
        /// <summary>
        /// Exécution d'une requete parmettant la récupération d'un champ d'une table avec ou sans doublons
        /// </summary>
        /// <param name="nomChamp">nom duy champ à sortir</param>
        /// <param name="table">nom de la table contenant le champ souhaité</param>
        /// <param name="accepterDoublons">bolléen indiquant si on souhaite garder les doublons (true) ou non (false)</param>
        /// <returns>une liste contenant les éléments du champ recus par la requete</returns>
        public List<string> GetChamp(string nomChamp, string table, bool accepterDoublons)
        {
            string doublons = " ";
            if (!accepterDoublons)
            {
                doublons = " DISTINCT ";
            }
            string chaineRequete = "SELECT" + doublons + nomChamp + " FROM " + table;

            this.commande = new MySqlCommand(chaineRequete, this.connexion);
            this.curseur = this.commande.ExecuteReader();

            List<string> champ = new List<string>();
            while (curseur.Read())
            {              
                champ.Add(this.curseur[nomChamp].ToString());
                Console.WriteLine(this.curseur[nomChamp].ToString());
            }
            curseur.Close();
            return champ;
        }


        // Ferme la connexion à la base de données pour na pas saturer le système et le serveur
        /// <summary>
        /// Ferme la connexion à la base de données pour na pas saturer le système et le serveur
        /// </summary>
        public void FermerConnexion()
        {
            this.connexion.Close();
        }

    }
}
